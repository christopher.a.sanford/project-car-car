import React, {useEffect, useState} from 'react';


function CreateAppointment () {

    const [vin,setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };

    const[customer,setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const[dateTime,setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    };

    const[technician,setTechnician] = useState(0);
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    };

    const [technicians, setTechnicians] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        };
      };

    const[reason,setReason] = useState('');
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.date_time = dateTime;
        data.reason = reason;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;

        const techniciansURL = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(techniciansURL, fetchConfig);
        if (response.ok){
            setVin('');
            setCustomer('');
            setDateTime('');
            setTechnician('');
            setReason('');
            event.target.reset();
        };
    };

    useEffect(() => {fetchData();}, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <form
            id="create-appointment-form"
            onSubmit = {handleSubmit}
            >
              <div className="form-floating mb-3">
                <input
                onChange={handleVinChange}
                value={vin}
                placeholder="VIN"
                required type="text"
                name="vin"
                id="vin"
                className="form-control"
                maxLength='17'
                />
                <label htmlFor="vin">Automobile VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleCustomerChange}
                value={customer}
                placeholder="Last name"
                required type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                />
                <label htmlFor="last_name">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleDateTimeChange}
                value={dateTime}
                placeholder="YYYY-MM-DDTHH:MM"
                required type="text"
                name="date_time"
                id="date_time"
                className="form-control"
                />
                <label htmlFor="date_time">Date and Time YYYY-MM-DDTHH:MM</label>
              </div>
              <div className="mb-3">
                <select
                onChange={handleTechnicianChange}
                value={technician}
                required name="technician"
                id="technician"
                className="form-select"
                >
                  <option value="">Choose a technician</option>
                  {technicians.map(technician => {
                    return (
                        <option key={technician['id']} value={technician['id']}>
                            {technician['employee_id']}
                        </option>
                    )
                  }) }
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleReasonChange}
                value={reason}
                placeholder="Reason"
                required type="text"
                name="reason"
                id="reason"
                className="form-control"
                />
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}






export default CreateAppointment;
