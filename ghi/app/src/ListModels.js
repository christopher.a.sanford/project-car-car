import React, { useState, useEffect } from 'react';

function ListModels() {

    const [models, setModels] = useState([]);
    const fetchData = async () => {
        const getURL = 'http://localhost:8100/api/models/';
        const response = await fetch(getURL);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        };
      };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => (
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td>
                            <img src={model.picture_url} alt={`${model.style} hat`} style={{width: "100px"}} />
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ListModels;
